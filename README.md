# CaptionVideoWebApp 


#install environment for window

1) install Anaconda on your PC

2) conda create -n <your name environment> python=3.6 anaconda

3) conda activate <your name environment>

4) conda install spyder

5) conda install theano

6) pip install tensorflow==1.14

7) conda install -c conda-forge keras

8) pip install keras==2.2.5

9) conda install opencv

10) pip install moviepy 

change directory to this folder

python app.py

open your web browser and go test
